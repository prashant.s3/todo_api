const mysql = require('mysql');
const { promisify } = require('util');
const config = require('config');

exports.getalldata = async (req, res) => {

    try{
    var connection = mysql.createConnection(config.get('db'));
  
    const result = await promisify(connection.query).call(
      connection,
      `call faq.Get_All_TODO();
      `,
      []
    );
  
    res.send({ slug: result[0] });
      
    }catch (err){

    console.log(err);
    res.status(404).json({
      status: 'error',
      statusCode: 404,
      error: 'No able to get data'
    });
  }finally {
    connection.end();
  }
}

exports.insertdata = async(req, res) => {

 try{
    var connection = mysql.createConnection(config.get('db'));
  
    let { name, task , status} = req.body;
  
  
     await promisify(connection.query).call(
      connection,
      `call faq.Insert_data_TODO('${name}','${task}','${status}');`,
      []
    );
  
    res.status(200).json({ message: "Data successfully inserted" });
     }catch (err){

        console.log(err);
        res.status(404).json({
          status: 'error',
          statusCode: 404,
          error: 'No able to insert data'
        });
      }finally {
        connection.end();
      }
  }

  exports.deletedata = async(req, res) => {

     try{

    var connection = mysql.createConnection(config.get('db'));
  
    let { id } = req.body;
  
     await promisify(connection.query).call(
      connection,
      `call faq.Delete_data_TODO(${id});`,
      []
    );
  
    res.status(200).json({ message: "Data successfully Deleted" });
  
  }catch (err){

    console.log(err);
    res.status(404).json({
      status: 'error',
      statusCode: 404,
      error: 'No able to delete data'
    });
  }finally {
    connection.end();
  }
}

exports.updatedata = async(req, res) => {
try{
    var connection = mysql.createConnection(config.get('db'));
    let id = req.params.ID;
    let{task, status} = req.body;
  
     await promisify(connection.query).call(
      connection,
      `call faq.update_TODO(${id},'${task}', '${status}');`,
      []
    );
  
    res.status(200).json({ message: "Data successfully Updated" });
  
  }catch (err){

    console.log(err);
    res.status(404).json({
      status: 'error',
      statusCode: 404,
      error: 'No able to get data'
    });
  }finally {
    connection.end();
  }
}

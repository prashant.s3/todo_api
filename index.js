const http = require('http');
const express = require('express');
const config = require('config');
const handler = require('./controllers/todohandler');
const app = express();

app.use(express.json());

app.get('/todo', handler.getalldata );

app.post('/todo/insertingdata', handler.insertdata );

app.delete('/Delete',handler.deletedata );

app.patch('/update/:ID',handler.updatedata);


const server = http.createServer(app);

server.listen(config.get('PORT'), () => console.log(`server is ready `));

